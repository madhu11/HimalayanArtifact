<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'ha');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'xGt5r$`9a%v&P*j`0I[M3YQv%|S(a.%X}aq^[vGw03+7D%^;BfTe>J+9qbh6KBmB');
define('SECURE_AUTH_KEY',  'hX0y-v9WE6K77FYoL>T4<CO?fjt4sBwtD<we=ZEs_1EvnYJN7Ff/N37ZRh(!pS+P');
define('LOGGED_IN_KEY',    'eQ{KV>S4)~6Jqe(aLJ}S{Jh2(lGzzqG;b6&^Jw)g8CUnx/eo;Od9pc0r+nOF~3sN');
define('NONCE_KEY',        '*0~NC/avPx+tO+(OsS[cUa:$0E)00)Q+hc?<e>>Ii%8HI6!w_uulEz|81(d6?fk:');
define('AUTH_SALT',        'l]5XmqQ{9ISkSPp@yD9Ih&Bt+#yB/oSjWs6MbjLkK;4w1tLbAY7?RWNIq6Y5&cU`');
define('SECURE_AUTH_SALT', 'D(p[gxMZFMkJ_{=3Ut,WPSv^G!oBr!tML ,zf|Y>v*J52].tzkOr1zQ`]A#FW5gg');
define('LOGGED_IN_SALT',   'npTQ;FV:<u|&t7u@_$+]XKs9z|o9la>Ojo#5y]&Y`2H/Eu^ExB9 qa}-QO7dO(r2');
define('NONCE_SALT',       'v}[&z|24o/0=/Ma:Y]ixs7|6E 1i>_trbp5T<F0YRDR<[ivq=z<*-rtO*_>>/)]>');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
